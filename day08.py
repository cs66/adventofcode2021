a = [[c.split(" ") for c in b.split(" | ")] for b in open("input08.txt").read().split("\n") if len(b)>1]
ssd = [
    "abcefg", #0
    "cf",     #1
    "acdeg",  #2
    "acdfg",  #3
    "bcdf",   #4
    "abdfg",  #5
    "abdefg", #6 
    "acf",    #7
    "abcdefg",#8
    "abcdfg"  #9
]
segFreq = dict((a,len([x for x in ssd if a in x])) for a in 'abcdefg')
def flat(ls):
    return [a for b in ls for a in b]
# Count 1,4,7,8
b = [[c for c in b[1] if len(c) in [2,4,3,7]] for b in a]

def srt(s):
    ret = list(s)
    ret.sort()
    return "".join(ret)

def decode(digits):
    one = set([a for a in digits if len(a) == 2][0])
    seven = set([a for a in digits if len(a)==3][0])
    eight = set([a for a in digits if len(a)==7][0])
    def f(s):
        t = list(s)
        t.sort()
        return "".join(t)
    ret = {}
    for x in seven - one:
        ret['a'] = x
    sft = dict((a,len([x for x in digits if a in x])) for a in 'abcdefg')
    ret['e'] = [z for z in sft if sft[z]==4][0]
    ret['f'] = [z for z in sft if sft[z]==9][0]
    ret['b'] = [z for z in sft if sft[z]==6][0]
    #segFreq = {'a': 8, 'b': 6, 'c': 8, 'd': 7, 'e': 4, 'f': 9, 'g': 7}
    #c is seg in one that is not f
    for x in one:
        if x != ret['f']:
            ret['c'] = x
    #zero has 6 segments
    zeroc = [a for a in digits if len(a) == 6]
    #zero has seg c (unlike 6)
    zeroc = [x for x in zeroc if ret['c'] in x]
    #zero has seg e (unlike 9)
    zeroc = [x for x in zeroc if ret['e'] in x]
    # b is the seg in zero not already found
    mustbeb = [s for s in zeroc[0] if not s in ret.values()]
    for seg in zeroc[0]:
        if not seg in ret.values():
            ret['g'] = seg
    #only d is missing - it is the seg in eight not found
    for seg in eight:
        if not seg in ret.values():
            ret['d'] = seg
    iret = dict((b,a) for (a,b) in ret.items())
    return iret

ssdd = dict((x,i) for i,x in enumerate(ssd))
def ssd2digit(s,code):
    ret = [code[a] for a in s]
    ret.sort()
    return ssdd["".join(ret)]
acc = 0
for grp in a:
    code = decode(grp[0])
    p = []
    for s in grp[1]:
        m = ssd2digit(s,code)
        p.append(f"{m}")
    num = int("".join(p))
    acc += num
print(acc)
