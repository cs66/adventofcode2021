import re
ls = open("input04.txt").read().split("\n\n")
nums = [x for x in ls[0].split(",")]
boards = [[re.split('\\s+', x) for x in y.split("\n") if len(x)>1] for y in ls[1:] if len(y)>1]
boards = [[[z for z in y if z!=''] for y in x if len(y)>0] for x in boards]
index = dict((x,i) for i,x in enumerate(nums))
whn = [[[index[z] for z in y] for y in x] for x in boards]
wht = [[[x[j][i] for j,z in enumerate(y)] for i,y in enumerate(x)] for x in whn]
def win(x):
    return min(max(y) for y in x)

whenWon = [min(win(x),win(y)) for x,y in zip(whn,wht)]
#the position of the lowest value in a indicates the winning board
winningIndex = min(whenWon)
winningBoard = boards[whenWon.index(winningIndex)]
def called(index):
    return set(x for x in nums[:index+1])
def uncalled(board,index):
    return [y for x in board for y in x if not y in called(index)]
s = sum(int(x) for x in uncalled(winningBoard,winningIndex))
print(f"Part A {s*int(nums[winningIndex])}")
losingIndex = max(whenWon)
lastNum = int(nums[losingIndex])
losingBoard = boards[whenWon.index(losingIndex)]
sumUncalled = sum(int(x) for x in uncalled(losingBoard,losingIndex))
print(f"whenWon {whenWon}\nlastNum {lastNum}\nsumUncalled {sumUncalled}")
print(f"Part B {sumUncalled*lastNum}")
