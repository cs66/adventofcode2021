G=[]
for line in open('15test.txt'):
    G.append([int(x) for x in line.strip()])
print(len(G))
DP = {}
def solve(r,c):
    if (r,c) in DP:
        return DP[(r,c)]
    if r>= len(G) or c>=len(G[r]):
        return 1e9
    if r==len(G)-1 and c==len(G[r])-1:
        return G[r][c]
    ans = G[r][c] + min(solve(r+1,c),solve(r,c+1))
    DP[(r,c)] = ans
    return ans

print(solve(0,0)-G[0][0])
#449 too low
#494 too high
#not 493