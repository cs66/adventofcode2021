import math
import sys
A = None
for r in open(sys.argv[1]):
    print(r)
    A = [x.split('=')[1] for x in r.strip()[13:].split(", ")]
[[xl,xh],[yl,yh]] = [[int(x) for x in x.split('..')] for x in A]

def tp(xv,yv):
    x = 0
    y = 0
    xvn = xv
    yvn = yv
    ret = []
    while y > yl:
        ret.append((x,y))
        x += xvn
        y += yvn
        xvn = max(xvn-1,0)
        yvn = yvn - 1
    return ret

def allYV():
    ret = []
    for yv in range(-500,500):
        y = 0
        v = yv
        t = 0
        while y>=yl and t<300:
            if yl<= y and y<=yh:
                ret.append((t,yv))
            y += v
            v -= 1
            t = t+1
    return ret

yvs = allYV()
maxT = max([t for t,y in yvs])

def allXV():
    ret = []
    for xv in range(int(math.sqrt(xl))-1,xh+1):
        x = 0
        v = xv
        t = 0
        while x<=xh and t<=maxT:
            if xl<= x and x<=xh:
                ret.append((t,xv))
            x += v
            v = max(v-1,0)
            t = t+1
    return ret

xvs = allXV()
xhh = dict([(t,[]) for t,v in xvs])
for t,v in xvs:
    xhh[t].append(v)

ret = set()
for t,yv in yvs:
    for xv in xhh[t]:
        ret.add(f"{xv},{yv}")

print(ret)

s = '''23,-10  25,-9   27,-5   29,-6   22,-6   21,-7   9,0     27,-7   24,-5
25,-7   26,-6   25,-5   6,8     11,-2   20,-5   29,-10  6,3     28,-7
8,0     30,-6   29,-8   20,-10  6,7     6,4     6,1     14,-4   21,-6
26,-10  7,-1    7,7     8,-1    21,-9   6,2     20,-7   30,-10  14,-3
20,-8   13,-2   7,3     28,-8   29,-9   15,-3   22,-5   26,-8   25,-8
25,-6   15,-4   9,-2    15,-2   12,-2   28,-9   12,-3   24,-6   23,-7
25,-10  7,8     11,-3   26,-7   7,1     23,-9   6,0     22,-10  27,-6
8,1     22,-8   13,-4   7,6     28,-6   11,-4   12,-4   26,-9   7,4
24,-10  23,-8   30,-8   7,0     9,-1    10,-1   26,-5   22,-9   6,5
7,5     23,-6   28,-10  10,-2   11,-1   20,-9   14,-2   29,-7   13,-3
23,-5   24,-8   27,-9   30,-7   28,-5   21,-10  7,9     6,6     21,-5
27,-10  7,2     30,-9   21,-8   22,-7   24,-9   20,-6   6,9     29,-5
8,-2    27,-8   30,-5   24,-7'''.split()
def ftr(ls):
    return [(x,y) for (x,y) in ls if x>=xl and x<=xh and y>=yl and y<=yh]
print(tp(7,2))
print(tp(6,3))
p = tp(15,15)

def sigma(n):
    return n*(n+1)//2

def flat(lls):
    return [a for b in lls for a in b]


xc = [[t for (t,x) in [(t,sigma(xv)-sigma(xv-t)) for t in range(1,xv)] if x>=xl and x<=xh] for xv in range(15,xl)]

def impactTime(v):
    #time at which missile will be in the middle
    y = (yl+yh)/2
    a = -0.5
    b = v+0.5
    c = -y
    return (-b-math.sqrt(b*b-4*a*c))/2/a

def height(v,t):
    return t*(2*v+1-t)//2

def hit(v):
    t = impactTime(v)
    y0 = height(v,int(t))
    y1 = height(v,int(t)+1)
    if (y0<=yh and y0>=yl):
        return int(t)
    if (y1<=yh and y1>=yl):
        return int(t)+1
    return None

ys = []
for vy in range(-300,300):
    t = hit(vy)
    if t:
        ys.append((t,vy))

print(ys)

def hit2(v):
    ret = []
    t = int(impactTime(v))
    i = 0
    y = height(v,t-i)
    while (y<=yh and y>=yl):
        ret.append(t-i)
        i+=1
        y = height(v, t-i)
    i = 1
    y = height(v,t+i)
    while (y<=yh and y>=yl):
        ret.append(t+i)
        i+=1
        y = height(v, t+i)
    return ret

ys2 = []
for vy in range(-300,300):
    t = hit2(vy)
    if t:
        ys2 += [(x,vy) for x in t]

vmin = int(math.sqrt(xl))

#9730 too low
#9870



