pl = [4,8]
pl = [2,8]
import json
dist = {}
for n in [a+b+c for a in range(1,4) for b in range(1,4) for c in range(1,4)]:
    dist[n] = dist.get(n,0) + 1
print(dist)
# Number of ways to get to (score1,score2,p1,p2,wt,goes)
M = [{((0,0),(pl[0],pl[1]),0,0):1}]
wins = [0,0]
def mo():
    MN = dict()
    for ((scores,posns,wt,goes),v) in M[-1].items():
        for (d,c) in dist.items():
            ns = list(scores)
            np = list(posns)
            np[wt] = (np[wt] + d - 1) % 10 +1
            ns[wt] += np[wt]
            if ns[wt] >= 21:
                wins[wt] += c*v
            else:
                k = (tuple(ns),tuple(np),1-wt,goes+1)
                MN[k] = MN.get(k,0) + c * v
    M.append(MN)
    return len(MN)

while mo() > 0:
    mo()

print(wins)
