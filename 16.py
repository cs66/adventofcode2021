import sys
print(sys.argv)
b = []
for r in open(sys.argv[1]):
    for c in r.strip():
        i = int(c,16)
        for j in range(4):
            b.append((i & 8)//8)
            i = i << 1
bs = "".join(str(i) for i in b)
print(bs)
ptr = 0
def fetch(n=1):
    global ptr
    ret = bs[ptr:ptr+n]
    ptr += n
    return ret

def peek(n=1):
    global ptr
    ret = bs[ptr:ptr+n]
    return ret

tvc = 0
hwm = 0
def packet():
    global tvc,hwm,ptr
    V = int(fetch(3),2)
    tvc += V
    T = int(fetch(3),2)
    ret = {"V":V,"T":T}
    if T == 4:
        ret["ls"] = []
        while peek() == '1':
            ret["ls"].append(fetch(5)[1:])
        ret["ls"].append(fetch(5)[1:])
        print(f"{ret=}")
        return ret
    #Must be an operator packet
    ret['spl'] = []
    if fetch() == '0':
        #total length in bits
        tlib = int(fetch(15),2)
        print(f"{tlib=}")
        stopAfter = tlib + ptr
        while ptr < stopAfter:
            ret['spl'].append(packet())
        return ret
    else:
        #number of sub packets
        nsp = int(fetch(11),2)
        for _ in range(nsp):
            ret['spl'].append(packet())
        return ret

p = packet()
print(p)
print(tvc)
def eval(t):
    T = t['T']
    if T == 4:
        return int("".join(t['ls']),2)
    sp = [eval(x) for x in t['spl']]
    if T == 0:
        #sum
        return sum(sp)
    if T == 1:
        #product
        ret = 1
        for i in sp:
            ret = ret*i
        return ret
    if T == 2:
        if len(sp) == 1:
            return sp[0]
        return min(*sp)
    if T == 3:
        if len(sp) == 1:
            return sp[0]
        return max(*sp)
    if T == 5:
        #greater
        return 1 if sp[0]>sp[1] else 0
    if T == 6:
        return 1 if sp[0]<sp[1] else 0
    if T == 7:
        return 1 if sp[0] == sp[1] else 0

    return None

print(eval(p))
#20505539 is too low :(
#12104213852056 is too low
