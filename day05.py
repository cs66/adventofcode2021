import math
def sign(x):
    if x==0:
        return 0
    if x<0:
        return -1
    return 1
rd = open("input05.txt").read().split("\n")[:-1]
pd = [[[int(k) for k in j.split(',')] for j in i.split(' -> ')] for i in rd]
pairs = [y for x in pd for y in x]
hz = [p for p in pd if p[0][0]==p[1][0] or p[0][1]==p[1][1]]
vc = {}
for p in hz:
    dxy = [sign(p[1][d] - p[0][d]) for d in [0,1]]
    for i in range(max(abs(p[0][0]-p[1][0]),abs(p[0][1]-p[1][1]))+1):
        pt = f"{[p[0][0]+i*dxy[0],p[0][1]+i*dxy[1]]}"
        vc[pt] = vc.get(pt,0) + 1
print(len([1 for p in vc if vc[p]>1]))

hz = pd
vc = {}
for p in hz:
    dxy = [sign(p[1][d] - p[0][d]) for d in [0,1]]
    for i in range(max(abs(p[0][0]-p[1][0]),abs(p[0][1]-p[1][1]))+1):
        pt = f"{[p[0][0]+i*dxy[0],p[0][1]+i*dxy[1]]}"
        vc[pt] = vc.get(pt,0) + 1
print(len([1 for p in vc if vc[p]>1]))


