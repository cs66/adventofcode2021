S = []
cs = None
for r in open('19.txt'):
    if r.startswith('---'):
        if cs:
            S.append(cs)
        cs = []
    else:
        if ',' in r:
            cs.append(tuple(int(x) for x in r.strip().split(',')))

S.append(cs)

ors = []
for o in [[0,1,2],[1,2,0],[2,0,1]]:
    for rev in [1,-1]:
        o1 = [*o]
        if rev == -1:
            o1.reverse()
        for x in [1,-1]:
            for y in [1,-1]:
                ors.append(([x,y,rev*x*y],o1))


def trf(o, disp = [0,0,0]):
    [x,y,z],[p,q,r] = o
    [i,j,k] = disp
    return lambda a: tuple([x*a[p]+i,y*a[q]+j,z*a[r]+k])

def sub(a,b):
    return tuple(x-y for x,y in zip(a,b))

def add(a,b):
    return tuple(x+y for x,y in zip(a,b))

def freq(ls):
    ret = dict()
    for x in ls:
        ret[x] = 1 + ret.get(x,0)
    return ret

def cmp(a,b):
    return [d for d,v in freq([sub(xa,xb) for xa in a for xb in b]).items() if v>=12]

def oo(a,b):
    for o in ors:
        tr = trf(o)
        ret = cmp(a,[tr(bx) for bx in b])
        if len(ret) == 1:
            return o,ret[0]

def ai(i,tr = None):
    inp = S[i]
    if tr:
        inp = [tr(x) for x in inp]
    a = [(j,oo(inp,x)) for x,j in [(S[j],j) for j in range(len(S)) if j!=i]]
    return [x for x in a if x[1]!=None]

transforms = {0:(ors[0],[0,0,0])}
dw = set()

def moveOn():
    global transforms
    ret = dict(transforms)
    for a in transforms:
        if not a in dw:
            dw.add(a)
            for b,o in ai(a,trf(*transforms[a])):
                ret[b] = o
    transforms = ret

while len(dw) < len(S):
    moveOn()

space = set()
for i in range(len(S)):
    tr = trf(*transforms[i])
    for ret in [tr(x) for x in S[i]]:
        space.add(ret)

print(len(space))

def mhd(a,b):
    return sum(abs(p-q) for p,q in zip(a,b))

sd = [x[1] for x in transforms.values()]
print(max([mhd(a,b) for a in sd for b in sd]))

#13803 too high
'''
theO=([1, -1, 1], [0, 1, 2])
theMatches=[(941, 235, -443), (1022, 236, -280), (851, -259, -1219), (1065, 180, 867), (1146, 181, 1030), (975, -314, 91),
 (-16, -16, 1299), (927, 148, -335), (1051, 141, -218), (1008, 149, -172), (837, -346, -1111), (-154, -48, 97), (81, 1, 163), (-140, -46, -34), (176, 459, 922), (133, 467, 968), (-38, -28, 29), (-1029, 270, 1237), (-908, 344, -87), (-944, 350,
-54), (316, 505, 956), (273, 513, 1002), (102, 18, 63), (-889, 316, 1271), (-768, 390, -53), (-43, 8, 46), (-214, -487, -893), (-1205, -189, 315)]
theTrB=[[686, -422, 578], [605, -423, 415], [515, -917, -361], [-336, -658, 858], [95, -138, 22], [-476, -619, 847], [-340
, 569, -846], [567, 361, 727], [-460, -603, -452], [669, 402, 600], [729, -430, 532], [-500, 761, 534], [-322, -571, 750],
 [-466, 666, -811], [-429, 592, 574], [-355, -545, -477], [703, 491, -529], [-328, 685, 520], [413, -935, -424], [-391, -539, -444], [586, 435, 557], [-364, 763, -893], [807, 499, -711], [755, 354, -619], [553, -889, -390]]
[(404, -588, -901), (-537, -823, -458)]
[[605, -423, 415], [-336, -658, 858]]
translate=(-201, -165, -1316)

def gaps(sm):
    ret = []
    for i in range(len(sm)-1):
        b = sm[i]
        for j in range(i+1,len(sm)):
            ret.append({'v':[b[d]-sm[j][d] for d in [0,1,2]], 's':sm[i],'t':sm[j]})
    return ret

def common(Sa,Sb):
    ref = dict([[f"{p['v']}",p] for p in gaps(Sa)])
    gb = gaps(Sb)
    ret = []
    for oi in range(len(ors)):
        o = ors[oi]
        c = 0
        disp = None
        tr = trf(o)
        for u in gb:
            dispA = tr(u['v'])
            if f"{dispA}" in ref:
                apair = ref[f"{dispA}"]
                c+=1
                if disp == None and c>=12:
                    print(f"Found matching pair {c}\n{dispA=}")
                    print(f"{apair=}")
                    bpair = u
                    print(f"{bpair=}")
                    disp = sub(apair['s'],tr(bpair['s']))
        if c>=12:
            ret.append((o,disp))
    if len(ret) == 1:
        return ret[0]
    if len(ret)>1:
        print("******************** wtf")
    return

import json
def addIn(Sa,Sb):
    ret = set(f"{x}" for x in Sa)
    odisp = common(Sa,Sb)
    if odisp:
        o,disp = odisp
        print(f"{disp=}")
        tr = trf(o,disp)
        for x in Sb:
            ret.add(f"{tr(x)}")
    return [json.loads(x) for x in ret]

ret = S[0]
for s in range(1,len(S)):
    ret = addIn(ret,S[s])
print(len(ret))
'''