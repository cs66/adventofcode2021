def go(ls,mostCommon = True,i=0):
    if len(ls) == 1:
        return int(ls[0],2)
    d1 = [x for x in ls if x[i]=="1"]
    if (2*len(d1) >= len(ls)) == mostCommon:
        d1 = d1
    else:
        d1 = [x for x in ls if x[i]=="0"]
    return go(d1,mostCommon,i+1)


ls = open("input03.txt").read().split("\n")
pairs = [[0,0] for x in ls[0]]
for bs in ls:
    for i,d in enumerate(bs):
        pairs[i][int(d)] += 1
gamma = "".join("0" if a>b else "1" for a,b in pairs)
epsilon = "".join("0" if a<b else "1" for a,b in pairs)
print(int(gamma,2)*int(epsilon,2))
print(go(ls,True,0)*go(ls,False,0))

