s = [int(i) for i in open("input06.txt").read().split(",")]
n = 256
fstOff = 8
nxtOff = 6

mem = {}
def nfish(itt,dtg):
    global mem
    k = f"{itt} {dtg}"
    if k in mem:
        return mem[k]
    if dtg == 0:
        mem[k] = 1
        return mem[k]
    if itt > 0:
        mem[k] = nfish(itt-1,dtg-1)
        return mem[k]
    mem[k] = nfish(nxtOff,dtg-1) + nfish(fstOff,dtg-1)
    return mem[k]

print(sum(nfish(i,n) for i in s))
