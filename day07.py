p = [int(x) for x in open("input07.txt").read().split(',')]
costs = [sum([abs(i-a) for a in p]) for i in range(min(p),max(p)+1)]
print(min(costs))
def tri(x):
    return x*(x+1)/2
c2 = [sum([tri(abs(i-a)) for a in p]) for i in range(min(p),max(p)+1)]
print(min(c2))
