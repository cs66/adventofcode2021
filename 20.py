A = None
B= {}
r = None
ct = {".":'0','#':'1'}
for l in open("20.txt"):
    if A == None:
        A = [ct[x] for x in l.strip()]
        r = 0
    else:
        if len(l) > 2:
            for c,x in enumerate(l.strip()):
                B[(r,c)] = '1' if x == '#' else '0'
            r += 1
ib = [B,'0']

g = [(a,b) for a in [-1,0,1] for b in [-1,0,1]]

def enhance(ib):
    img = ib[0]
    inf = ib[1]
    loc = min(x for x,y in img)
    hic = max(x for x,y in img)
    lor = min(y for x,y in img)
    hir = max(y for x,y in img)
    def pix(r,c):
        if c>=loc and c<=hic and r>=lor and r<=hir:
            if not (r,c) in img:
                print("Error",(r,c),(loc,hic,lor,hir))
                print(img)
            return img[(r,c)]
        else:
            return inf
    ret = {}
    for r0,c0 in [(r,c) for r in range(lor-1,hir+2) for c in range(loc-1,hic+2)]:
        bp = [pix(r0+dx,c0+dy) for dx,dy in g]
        i = int("".join(bp),2)
        ret[(r0,c0)] = A[i]
    return [ret,A[int(inf*9,2)]]

def bright(img):
    return sum(int(x) for x in img[0].values())

def show(img2):
    img = img2[0]
    lox = min(x for x,y in img)
    hix = max(x for x,y in img)
    loy = min(y for x,y in img)
    hiy = max(y for x,y in img)
    ret = []
    for x in range(loy,hiy+1):
        rw = []
        for y in range(lox,hix+1):
            v = img.get((x,y),'0')
            rw.append('#' if v == '1' else '.')
        ret.append("".join(rw))
    print("\n".join(ret))

ret = bright(enhance(enhance(ib)))
print(ret)

ret = ib
for i in range(50):
    ret = enhance(ret)
print(bright(ret))
#5752 too high
#5751 too high
#5419 too low
#5661 wrong
