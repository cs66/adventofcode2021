def label(t,li,di):
    if isinstance(t,int):
        return {"v":t,"li":li,"di":di,"lin":li+1}
    lf = label(t[0],li,       di+1)
    rt = label(t[1],lf["lin"],di+1)
    return {
        "v":[lf,rt],
        "di": di,
        "lin":rt["lin"]
    }

def findBomb(t):
    if t["di"] == 4 and not isinstance(t["v"], int):
        return t
    if isinstance(t["v"],int):
        return None
    ret = findBomb(t["v"][0])
    if ret:
        return ret
    else:
        return findBomb(t["v"][1])


def explode(t):
    def pop(t,la,ra,idx,lin):
        if "li" in t and t["li"] == idx - 1:
            return t["v"] + la
        if "li" in t and t["li"] == idx + 2:
            return t["v"] + ra
        if t["lin"] == lin and t["di"] == 4:
            return 0
        v = t["v"]
        if isinstance(v,int):
            return v
        return [pop(v[0],la,ra,idx,lin),pop(v[1],la,ra,idx,lin)]
    x = label(t,0,0)
    bomb = findBomb(x)
    la = bomb["v"][0]["v"]
    ra = bomb["v"][1]["v"]
    idx = bomb["v"][0]["li"]
    lin = bomb["lin"]
    return pop(x,la,ra,idx,lin)

def becomes(a,b):
    return explode(a) == b

def split(t):
    if isinstance(t, int):
        if t>9:
            return {
                "happened":True,
                "ret": [t//2,t-t//2]
            }
        else:
            return {"happened":False,"ret":t}
    ret = {"happened":False,"ret":[]}
    for lr in [0,1]:
        s = {"happened":False, "ret":t[lr]}
        if not ret["happened"]:
            s = split(t[lr])
        ret["happened"] = ret["happened"] or s["happened"]
        ret["ret"].append(s["ret"])
    return ret

def add(a,b):
    ret = [a,b]
    changed = True
    while changed:
        changed = False
        x = label(ret,0,0)
        bomb = findBomb(x)
        if (bomb):
            changed = True
            ret = explode(ret)
        else:
            s = split(ret)
            if s["happened"]:
                changed = True
                ret = s["ret"]
    return ret

add([[[[4,3],4],4],[7,[[8,4],9]]] , [1,1])

def magnitude(t):
    if isinstance(t,int):
        return t
    return 3*magnitude(t[0])+2*magnitude(t[1])

p = [
    [[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]],
    [[[5,[2,8]],4],[5,[[9,9],0]]],
    [6,[[[6,2],[5,6]],[[7,6],[4,7]]]],
    [[[6,[0,7]],[0,9]],[4,[9,[9,0]]]],
    [[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]],
    [[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]],
    [[[[5,4],[7,7]],8],[[8,3],8]],
    [[9,3],[[9,9],[6,[4,9]]]],
    [[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]],
    [[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]
]

ret = None
for i in p:
    if ret == None:
        ret = i
    else:
        ret = add(ret,i)

print(magnitude(ret))
maxm = 0
for i in p:
    for j in p:
        if i != j:
            v = magnitude(add(i,j))
            maxm = max(maxm,v)

print(maxm)
