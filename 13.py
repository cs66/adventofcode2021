import json
ll = [x.split("\n") for x in open("14.txt").read().split("\n\n")]
dots = [[int(x) for x in x.split(",")] for x in ll[0]]
folds = [x[11:].split("=") for x in ll[1] if len(x)>0]
def foldy(lst,yf):
    #print("fold",yf)
    ret = set()
    for [x,y] in lst:
        #print([x,y])
        if y<yf:
            ret.add(f"[{x},{y}]")
        else:
            ret.add(f"[{x},{2*yf-y}]")
    return [json.loads(x) for x in list(ret)]
def foldx(lst,xf):
    ret = set()
    for [x,y] in lst:
        #print([x,y])
        if x<xf:
            ret.add(f"[{x},{y}]")
        else:
            ret.add(f"[{2*xf-x},{y}]")
    return [json.loads(x) for x in list(ret)]


for f in folds:
    if f[0] == 'y':
        print(f[1])
        dots = foldy(dots,int(f[1]))
    else:
        dots = foldx(dots,int(f[1]))
    print(len(dots))
xm = max([x for [x,y] in dots])
ym = max([y for [x,y] in dots])
h = dict((f"{x},{y}",1) for [x,y] in dots)
g = [['#' if f"{x},{y}" in h else ' ' for x in range(xm+1)] for y in range(ym+1)]
print("\n".join(["".join(r) for r in g]))
'''
0
1
2
3 -----
4 -> 2
5 -> 1
'''