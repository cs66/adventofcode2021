I = []
for l in open("22-2.txt"):
    v, r = l.strip().split(' ')
    p = [[int(y) for y in x[2:].split('..')] for x in r.split(',')]
    I.append({"v":v,"bounds":p})
print(I)

R = [[[0 for x in range(-50,51)] for y in range(-50,51)] for z in range(-50,51)]
for i in I:
    v = i["v"]
    g = i["bounds"]
    for x in range(max(-50,g[0][0]),min(50,g[0][1])+1):
        for y in range(max(-50,g[1][0]),min(50,g[1][1])+1):
            for z in range(max(-50,g[2][0]),min(50,g[2][1])+1):
                R[x][y][z] = 0 if v == 'off' else 1

s = sum(sum(sum(r for r in q) for q in p) for p in R)
print(s)

cfe = 0

def ons(ttd,default,lox,hix,loy,hiy,loz,hiz):
    print(len(ttd),default,lox,hix,loy,hiy,loz,hiz)
    global cfe
    if hix<lox or hiy<loy or hiz<loz:
        return 0
    if len(ttd) == 0:
        if default == "off":
            return 0
        else:
            score = (1+hix-lox)*(1+hiy-loy)*(1+hiz-loz)
            print(f"{score=}")
            return score
    print(len(ttd),ttd[0],default,lox,hix,loy,hiy,loz,hiz)
    cfe += 1
    if len(ttd)>9:
        print(cfe,len(ttd))
    hd = ttd[0]
    tl = ttd[1:]
    bnds = hd['bounds']
    #outside to the left and right
    print(len(ttd),"left")
    ret =  ons(tl,default,lox,bnds[0][0]-1,loy,hiy,loz,hiz)
    print(len(ttd),"right")
    ret += ons(tl,default,bnds[0][1]+1,hix,loy,hiy,loz,hiz)
    #outside bottom and top
    ret += ons(tl,default,bnds[0][0]+1,bnds[0][1]-1,loy,bnds[1][0]-1,loz,hiz)
    ret += ons(tl,default,bnds[0][0]+1,bnds[0][1]-1,bnds[1][1]+1,hiy,loz,hiz)
    #front and back
    ret += ons(tl,default,bnds[0][0]+1,bnds[0][1]-1,bnds[1][0]+1,bnds[1][1]-1,loz,bnds[2][0]-1)
    ret += ons(tl,default,bnds[0][0]+1,bnds[0][1]-1,bnds[1][0]+1,bnds[1][1]-1,bnds[2][1]+1,hiz)
    #now the inside of this cuboid
    if bnds[0][0]>hix or bnds[0][1]<lox or bnds[1][0]>hiy or bnds[1][1]<loy or bnds[2][0]>hiz or bnds[2][1]<loz:
        pass
    else:
        ret += ons(tl,hd['v'],bnds[0][0],bnds[0][1],bnds[1][0],bnds[1][1],bnds[2][0],bnds[2][1])
    return ret

ret = ons(I,"off",-1e9,1e9,-1e9,1e9,-1e9,1e9)
print(ret)
