ll = open('14.txt').read().split('\n')
s=ll[0]
rules = dict([a.split(' -> ') for a in ll[2:-1]])
def apply(s):
    ret = [s[0]]
    for i in range(len(s)-1):
        if s[i:i+2] in rules:
            ret.append(rules[s[i:i+2]])
        ret.append(s[i+1])
    return "".join(ret)

for i in range(10):
    print(s)
    s = apply(s)

freq = {}
for c in s:
    freq[c] = freq.get(c,0) + 1
print(freq)
ret = list(freq.values())
ret.sort()
print(ret[-1]-ret[0])

n = 40
pr = dict()
# Character count after running the pair AB for 12 iteration..
# {"AB-12": {"A":100, "B": 123, "C": 321}} for example
def sumFreq(X,Y):
    ret = dict(X)
    for y in Y:
        ret[y] = ret.get(y,0) + Y[y]
    return ret
#Return character count of running pai="AB" for n iteration, do not count the final B
def runPair(pair,n):
    #print(pair,n)
    if n==0:
        return dict([(pair[0],1)])
    k = f"{pair}-{n}"
    if k in pr:
        return pr[k]
    middle = rules[pair]
    ret = sumFreq(runPair(pair[0]+middle,n-1),runPair(middle+pair[1],n-1))
    pr[k] = ret
    return ret

def proc(s,n):
    ret = dict([(s[-1],1)])
    for i in range(len(s)-1):
        ret = sumFreq(ret,runPair(s[i:i+2],n))
    return ret

ret = list(proc(ll[0],40).values())
ret.sort()
print(ret[-1]-ret[0])

    
