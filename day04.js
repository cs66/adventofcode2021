let fs = require('fs');
let boards, calls,timeToWin;
fs.readFile('input04.txt','utf-8', (err,r)=>{
  let ls = r.split(/\r?\n\r?\n/);
  calls = ls.shift().split(',').map(i=>parseInt(i));
  sllac = Object.fromEntries(calls.map((x,i)=>[x,i]));
  boards = ls.map(b=>{
    return b.split(/\r?\n/).filter(s=>s.length>0).map(r=>{
      return r.split(/\s+/).filter(s=>s.length>0).map(n=>sllac[n]);
    })
  });
  timeToWin = boards.map(x=>{
    return Math.min(
      ...x.map(r=>Math.max(...r)),
      ...transpose(x).map(r=>Math.max(...r)));
  });
  console.log(timeToWin);
});

function transpose(a){
  return a[0].map((_,i)=>{
    return a.map((_,j)=>{
      return a[j][i];
    })
  })
}
transpose(b);

